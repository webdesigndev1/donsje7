<?php


namespace Jworks\ConfigurableProduct\Plugin\Model;

class AttributeOptionProvider
{

    /**
     * {@inheritdoc}
     */
    public function afterGetAttributeOptions(\Magento\ConfigurableProduct\Model\AttributeOptionProvider $subject, array $result)
    {
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();        
 
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        
        $currId = $storeManager->getStore()->getStoreId();
        foreach ($result as &$option) {

            if(isset($option['stock_status']) && $option['stock_status']==0){
                if ($currId == 2) {
                   $option['option_title']  = $option['option_title'].__(' - Niet op voorraad');
                }
                else{
                    $option['option_title']  = $option['option_title'].__(' - Out of stock');
                }
            }
        }
        return $result;
    }
}
