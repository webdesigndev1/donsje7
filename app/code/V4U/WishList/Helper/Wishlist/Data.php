<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace V4U\WishList\Helper\Wishlist;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\Data\Form\FormKey;
use Magento\Framework\App\PageCache\FormKey as PageCacheFormKey;
use Magento\Framework\Session\Config\ConfigInterface;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
/**
 * Wishlist Data Helper
 *
 * @author     Magento Core Team <core@magentocommerce.com>
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 *
 * @api
 * @since 100.0.2
 */
class Data extends \Magento\Wishlist\Helper\Data
{
   
    public function refreshFormKey()
        {
             $beforeWishlistRequest = $this->_customerSession->getBeforeWishlistRequest();


        if ($beforeWishlistRequest !== null) {

            $formKeyData = ObjectManager::getInstance()->get(FormKey::class);

            //form_key is already empty
            $formKey = $formKeyData->getFormKey();

            $sessionConfig = ObjectManager::getInstance()->get(ConfigInterface::class);
            $cookieMetadata = ObjectManager::getInstance()->get(CookieMetadataFactory::class)->createPublicCookieMetadata();
            $cookieMetadata->setDomain($sessionConfig->getCookieDomain());
            $cookieMetadata->setPath($sessionConfig->getCookiePath());
            $cookieMetadata->setDuration($sessionConfig->getCookieLifetime());

            ObjectManager::getInstance()->get(PageCacheFormKey::class)->set(
                $formKey,
                $cookieMetadata
            );

            $beforeWishlistRequest['form_key'] = $formKey;
            $this->_customerSession->setBeforeWishlistRequest($beforeWishlistRequest);
            $this->_customerSession->setBeforeRequestParams($beforeWishlistRequest);
            $this->_customerSession->setBeforeModuleName('wishlist');
            $this->_customerSession->setBeforeControllerName('index');
            $this->_customerSession->setBeforeAction('add');
        }

        return $this;
    }
}
