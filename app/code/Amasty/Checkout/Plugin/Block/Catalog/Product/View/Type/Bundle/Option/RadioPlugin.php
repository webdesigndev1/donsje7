<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Checkout
 */


namespace Amasty\Checkout\Plugin\Block\Catalog\Product\View\Type\Bundle\Option;

use Magento\Bundle\Block\Catalog\Product\View\Type\Bundle\Option\Radio;

/**
 * Class RadioPlugin
 */
class RadioPlugin
{
    /**
     * Fix fatal on our checkout for magento 2.3.2 and 2.2.9
     * 'Call to a member function renderTierPrice() on null',
     * when bundle product in cart. Because catalog_product_view_type_bundle.xml set
     * argument tier_price_renderer, but on our checkout we dont use this layout.
     *
     * @param Radio $subject
     */
    public function beforeGetData(
        Radio $subject
    ) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        if (class_exists(\Magento\Bundle\Block\DataProviders\OptionPriceRenderer::class)) {
            $optionPriceRenderer = $objectManager->get(\Magento\Bundle\Block\DataProviders\OptionPriceRenderer::class);
            $subject->setTierPriceRenderer($optionPriceRenderer);
        }
    }
}
