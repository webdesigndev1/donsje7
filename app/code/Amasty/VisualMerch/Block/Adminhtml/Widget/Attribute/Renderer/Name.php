<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_VisualMerch
 */


namespace Amasty\VisualMerch\Block\Adminhtml\Widget\Attribute\Renderer;

class Name extends \Amasty\VisualMerch\Block\Adminhtml\Widget\Attribute\Renderer
{
    /**
     * @param bool $useTitle
     * @return string
     */
    public function render($useTitle = true)
    {
        return parent::render(false);
    }
}
