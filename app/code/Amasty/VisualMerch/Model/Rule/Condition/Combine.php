<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_VisualMerch
 */


namespace Amasty\VisualMerch\Model\Rule\Condition;


class Combine extends \Magento\CatalogRule\Model\Rule\Condition\Combine
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    private $excludedAttributes = ['category_ids'];

    /**
     * @var array
     */
    private $operatorsMap = [
        '==' => '!=',
        '!=' => '==',
        '>=' => '<',
        '<=' => '>',
        '>' => '<=',
        '<' => '>=',
        '{}' => '!{}',
        '!{}' => '{}',
        '()' => '!()',
        '!()' => '()',
    ];

    public function __construct(
        \Magento\Rule\Model\Condition\Context $context,
        \Amasty\VisualMerch\Model\Rule\Condition\ProductFactory $conditionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $data = []
    ) {
        parent::__construct($context, $conditionFactory, $data);
        $this->storeManager = $storeManager;
        $this->setType('Amasty\VisualMerch\Model\Rule\Condition\Combine');
    }

    public function getNewChildSelectOptions()
    {
        $productAttributes = $this->_productFactory->create()->loadAttributeOptions()->getAttributeOption();

        $attributes = [];
        foreach ($productAttributes as $code => $label) {
            if (!in_array($code, $this->excludedAttributes)) {
                $attributes[] = [
                    'value' => 'Amasty\VisualMerch\Model\Rule\Condition\Product|' . $code,
                    'label' => $label,
                ];
            }
        }
        $conditions = [['value' => '', 'label' => __('Please choose a condition to add.')]];
        $conditions = array_merge_recursive(
            $conditions,
            [
                [
                    'value' => 'Amasty\VisualMerch\Model\Rule\Condition\Combine',
                    'label' => __('Conditions Combination'),
                ],
                [
                    'label' => __('Custom Fields'),
                    'value' => array(
                        array(
                            'label' => __('Is New (by a period)'),
                            'value' => 'Amasty\VisualMerch\Model\Rule\Condition\IsNewByPeriod',
                        ),
                        array(
                            'label' => __('Is New (by \'is_new\' attribute)'),
                            'value' => 'Amasty\VisualMerch\Model\Rule\Condition\IsNew',
                        ),
                        array(
                            'label' => __('Created (in days)'),
                            'value' => 'Amasty\VisualMerch\Model\Rule\Condition\Created',
                        ),
                        array(
                            'label' => __('In Stock'),
                            'value' => 'Amasty\VisualMerch\Model\Rule\Condition\InStock',
                        ),
                        array(
                            'label' => __('Is on Sale'),
                            'value' => 'Amasty\VisualMerch\Model\Rule\Condition\Price\Sale',
                        ),
                        array(
                            'label' => __('Qty'),
                            'value' => 'Amasty\VisualMerch\Model\Rule\Condition\Qty',
                        ),
                        array(
                            'label' => __('Min Price'),
                            'value' => 'Amasty\VisualMerch\Model\Rule\Condition\Price\Min',
                        ),
                        array(
                            'label' => __('Max Price'),
                            'value' => 'Amasty\VisualMerch\Model\Rule\Condition\Price\Max',
                        ),
                        array(
                            'label' => __('Final Price'),
                            'value' => 'Amasty\VisualMerch\Model\Rule\Condition\Price\FinalPrice',
                        ),
                        array(
                            'label' => __('Rating'),
                            'value' => 'Amasty\VisualMerch\Model\Rule\Condition\Rating',
                        ),
                    )
                ],
                ['label' => __('Product Attribute'), 'value' => $attributes]
            ]
        );

        return $conditions;
    }

    public function collectConditionSql()
    {
        $wheres = [];
        foreach ($this->getConditions() as $condition) {
            $where = $condition->collectConditionSql();
            if ($where) {
                $wheres[] = $where;
            }

        }

        if (empty($wheres)) {
            return '';
        }

        $delimiter = $this->getAggregator() == "all" ? ' AND ' : ' OR ';
        return '(' . implode($delimiter, $wheres) . ')';
    }

    /**
     * @param object $condition
     * @return $this
     */
    public function addCondition($condition)
    {
        $condition->setData('store_manager', $this->storeManager);
        return parent::addCondition($condition);
    }

    /**
     * @inheritdoc
     */
    public function collectValidatedAttributes($productCollection)
    {
        foreach ($this->getConditions() as $condition) {
            /** @var Product|Combine $condition */
            if (!$this->getValue()) {
                if ($condition instanceof Combine) {
                    $condition->setValue((int)!$condition->getValue());
                } else {
                    $condition->setOperator($this->operatorsMap[$condition->getOperator()]);
                }
            }
        }

        return parent::collectValidatedAttributes($productCollection);
    }
}
