<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_VisualMerch
 */


namespace Amasty\VisualMerch\Setup\Operation;

use Magento\Framework\Setup\SchemaSetupInterface;

class UpdateIndexerColumn
{
    public function execute(SchemaSetupInterface $setup)
    {
        $table = $setup->getTable('amasty_merchandiser_product_index_eav');
        $connection = $setup->getConnection();

        $connection->changeColumn(
            $table,
            'value',
            'value',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 5,
                'nullable' => false
            ]
        );
    }
}
