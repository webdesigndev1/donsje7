<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_VisualMerch
 */


namespace Amasty\VisualMerch\Controller\Adminhtml\Product;

/**
 * Class Save
 * @package Amasty\VisualMerch\Controller\Adminhtml\Product
 */
class Save extends ControllerAbstract
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Amasty\VisualMerch\Model\RuleFactory
     */
    protected $ruleFactory;

    /**
     * @var \Amasty\Base\Model\Serializer
     */
    protected $serializer;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \Magento\Framework\View\LayoutFactory $layoutFactory,
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Amasty\VisualMerch\Model\Product\AdminhtmlDataProvider $dataProvider,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Amasty\VisualMerch\Model\RuleFactory $ruleFactory,
        \Amasty\Base\Model\Serializer $serializer
    ) {
        parent::__construct(
            $context,
            $resultRawFactory,
            $layoutFactory,
            $registry,
            $categoryRepository,
            $categoryFactory,
            $dataProvider,
            $resultJsonFactory
        );
        $this->ruleFactory = $ruleFactory;
        $this->serializer = $serializer;
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();

        $this->initCategory();

        $storeId = $this->getRequest()->getParam('store', $this->dataProvider->getStoreId());
        $this->dataProvider->setStoreId($storeId);

        $topProductData = $this->getRequest()->getParam('top_product_data', []);
        if (!empty($topProductData)) {
            $this->dataProvider->resortPositionData($topProductData['source_position'], 0);
            $this->dataProvider->setProductPositionData([$topProductData['entity_id'] => 0]);
        }

        $automaticProductData = $this->getRequest()->getParam('automatic_product_data', []);
        if (!empty($automaticProductData)) {
            $this->dataProvider->unsetProductPositionData($automaticProductData['entity_id']);
            $position = $this->dataProvider->getCurrentProductPosition($automaticProductData['entity_id']);
            $this->dataProvider->resortPositionData($automaticProductData['source_position'], $position);
        }

        $ruleData = $this->getRequest()->getParam('rule', []);
        if (isset($ruleData['conditions'])) {
            $rule = $this->ruleFactory->create();
            $rule->loadPost($ruleData);

            $serializedConditions = $this->serializer->serialize($rule->getConditions()->asArray());
            $this->dataProvider->setSerializedRuleConditions($serializedConditions);
            $this->dataProvider->setRestoreConditions(true);
        }

        $positions = $this->getRequest()->getParam('positions', []);
        $sortOrder = $this->getRequest()->getParam('sort_order', false);

        $this->dataProvider->setProductPositionData($positions);
        $this->dataProvider->setSortOrder($sortOrder);

        $resultJson->setData([]);
        return $resultJson;
    }
}
