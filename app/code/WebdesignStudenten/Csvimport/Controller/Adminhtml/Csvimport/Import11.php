<?php
namespace WebdesignStudenten\Csvimport\Controller\Adminhtml\Csvimport;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Backend\App\Action;


class Import extends \Magento\Backend\App\Action
{
    protected $_attribute;
    
    public function execute()
    {
	$resultRedirect = $this->resultRedirectFactory->create();
        $dir = $this->_objectManager->get('\Magento\Framework\App\Filesystem\DirectoryList');           
        $id = $this->getRequest()->getParam('csvimport_id');
        $model = $this->_objectManager->create('WebdesignStudenten\Csvimport\Model\Csvimport');
        $model->load($id);
        $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')
                        ->getDirectoryRead(DirectoryList::MEDIA);
        $file = $mediaDirectory->getAbsolutePath('webdesign-studenten/import').$model->getFile();                
		
		$data = fopen($file,"r");
		
		while(! feof($data))
		{
			$row = fgetcsv($data);
			if($row[1] =="sku"){
				$this->_attribute = $row;
			}
           
			if($row[1] !="sku" && $row[array_search ('product_type', $this->_attribute)] == 'Simple'){
				if(! $this->_objectManager->get('Magento\Catalog\Model\Product')->getIdBySku($row[1])){
					$this->createSimpleProduct($row, $this->_attribute);	
				}	  
			}
		}
		fclose($data);
		$dataconfig = fopen($file,"r");
		while(! feof(	$dataconfig))
		{
		    $row = fgetcsv(	$dataconfig);
			if($row[1] =="sku"){
				$this->_attribute = $row;
			}
			
			if($row[1] !="" && $row[array_search ('product_type', $this->_attribute)] == 'Configurable'){
				if(! $this->_objectManager->get('Magento\Catalog\Model\Product')->getIdBySku($row[1])){
					$this->createConfigureProduct($row, $this->_attribute);	
				}	  
			}
		}
		fclose($dataconfig);
		
        $this->messageManager->addSuccess(
			__('Successfully Product Imported')
		);
        return $resultRedirect->setPath('*/*/');
    }
    public function createConfigureProduct($Row, $att){
		
		$configureattribute = array();
		foreach(explode(',',$Row[array_search('configure_attribute', $this->_attribute)]) as $attribute){
			if($attribute){
				$configureattribute[] = $attribute;
			} 
		}
		
		$attributeidsarray = array();
		foreach($configureattribute as $attribute){
			$id = $this->getAttributeId($attribute);
			$attributeidsarray[] = $id;
		}
		if(count($attributeidsarray) > 0){
			$configurable_product = $this->_objectManager->create('\Magento\Catalog\Model\Product');
			$configurable_product->setSku($Row[array_search('sku', $this->_attribute)]);
			$configurable_product->setName($Row[array_search('Product name', $this->_attribute)]);
			$configurable_product->setAttributeSetId($this->getAttributeset($Row[array_search('attribute_set', $this->_attribute)]));
			$configurable_product->setStatus(1);
			$configurable_product->setTypeId('configurable');
			$configurable_product->setPrice($Row[array_search('price', $this->_attribute)]);
			
			$configurable_product->setWebsiteIds(array($Row[array_search('website_id', $this->_attribute)]));
			
			
			if($Row[array_search('additional_attributes', $this->_attribute)]){
    			$additionalAttributes = explode(',',$Row[array_search('additional_attributes', $this->_attribute)]);
    			if(count($additionalAttributes) > 0 ){
    				foreach($additionalAttributes as $additionalAttribute) {
    					$attribute = explode('=',$additionalAttribute);
    					if(count($attribute) > 0 ){ 
    						$entityType = 'catalog_product';
    						//$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    						$attributedata = $this->_objectManager->get(\Magento\Eav\Model\Entity\Attribute::class)->loadByCode($entityType, $attribute[0]);
    						if($attributedata->getFrontendInput()=='select'){
    							$configurable_product->setData($attribute[0],$this->getDropdownattributevalue($configurable_product,$attribute[0],trim($attribute[1])));
    						}
    						else if($attributedata->getFrontendInput()=='multiselect'){ 
    							$configurable_product->setData($attribute[0],$this->getMultiselectattributevalue($configurable_product,$attribute[0],trim($attribute[1])));
    						}
    						else{
    							$configurable_product->setData($attribute[0],$attribute[1]);
    						}
    					}
    				}
    			}
    		}
			
			$catIds = '';
			
			$_categoryFactory = $this->_objectManager->get('Magento\Catalog\Model\CategoryFactory');
			$cat = explode("|", $Row[array_search('categories', $this->_attribute)]);
			$parentId = '';
				foreach($cat as $title){
				$collection = $_categoryFactory->create()->getCollection();
				if($parentId){
					$collection->addFieldToFilter('parent_id', $parentId);
				}
				$collection->addFieldToFilter('name', ['in' => $title]);
				if ($collection->getSize()) {
						$categoryId = $collection->getFirstItem()->getId();
					
					if($catIds==''){
					$catIds = $categoryId;
					}else {
						$parentId = $categoryId;
						$catIds = $catIds.','.$categoryId;
					}
				}
				
			}

			$configurable_product->setCategoryIds(explode(',',$catIds));
			$configurable_product->setStockData(array(
				'use_config_manage_stock' => 0, 
				'manage_stock' => 1, 
				'is_in_stock' => 1,
				'is_in_stock' => 100,
				)
			);
			
			$configurable_product->getTypeInstance()->setUsedProductAttributeIds($attributeidsarray,$configurable_product);
			$configurableAttributesData = $configurable_product->getTypeInstance()->getConfigurableAttributesAsArray($configurable_product);
			$configurable_product->setCanSaveConfigurableAttributes(true);
			$configurable_product->setConfigurableAttributesData($configurableAttributesData);
			$configurableProductsData = array();
			
			
			$simpleproduct = array();
			$associateds = explode(",", $Row[array_search('associated_skus', $this->_attribute)]);
			foreach($associateds as $associated){
				
				$simpleproduct[] = $this->_objectManager->get('Magento\Catalog\Model\Product')->getIdBySku(trim(str_replace('"','',$associated)));
			}
			
			foreach($simpleproduct as $product){
				$a = 0;
				$attributeoption = array();
				foreach($configureattribute as $attribute){
					$attributeoption[$a] =  array(
						'label' => $this->getlabel($product,$attribute), //attribute label
						'attribute_id' => $this->getAttributeId($attribute), //attribute ID of attribute 'color' in my store
						'value_index' => $this->getvelueIndex($product,$attribute,$this->getlabel($product,$attribute)), //value of 'Green' index of the attribute 'color'
						'is_percent' => '0', //fixed/percent price for this option
						'pricing_value' => '' //value for the pricing
					);
					$a++;
				}
				
				$configurableProductsData[$product] = $attributeoption;
				
			}
			
			$configurable_product->setConfigurableProductsData($configurableProductsData);
			$configurable_product->setAssociatedProductIds($simpleproduct); // Assign simple product id
			$configurable_product->setCanSaveConfigurableAttributes(true);
			$configurable_product->save();
			$this->imageUplaod($Row,$configurable_product);
		}
	}
	public function getvelueIndex($product,$attribute,$label){
		
		$product = $this->_objectManager->create('Magento\Catalog\Model\Product')->load($product);	
		return $product->getData($attribute);
	}
	public function getlabel($product,$attribute){
		
		$product = $this->_objectManager->create('Magento\Catalog\Model\Product')->load($product);
		return $product->getAttributeText($attribute);	
	}
	public function getAttributesId($configureattribute) {
		$attribute = array();
		foreach($configureattribute as $atribute){
			
			$eavConfig = $this->_objectManager->get('Magento\Eav\Model\Config');
			$attribute = $eavConfig->getAttribute('catalog_product', $atribute);
			$attribute[] = $attribute->getId();
		}
		return $attribute;
	}	
	public function getAttributeId($attributecode) {
		
		$eavConfig = $this->_objectManager->get('Magento\Eav\Model\Config');
		$attribute = $eavConfig->getAttribute('catalog_product', $attributecode);
		return $attribute->getId();
	}

	public function getAttributeset($attrSetName){
		 $attributeset = $this->_objectManager->get('\Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory');
		 $attributeSet = $attributeset->create()->addFieldToSelect(
                    '*'
                    )->addFieldToFilter(
                            'attribute_set_name',
                            $attrSetName
                    )->addFieldToFilter(
                            'entity_type_id',
                            4
                    );
					
        $attributeSetId = 0;
        foreach($attributeSet as $attr):
            $attributeSetId = $attr->getAttributeSetId();
        endforeach;
        return $attributeSetId;
		
	}
	public function createSimpleProduct($Row, $attributeArray){
		$product = $this->_objectManager->create('\Magento\Catalog\Model\Product');
		$product->setWebsiteIds(array(1)); 
		$product->setSku($Row[array_search ('sku', $this->_attribute)]);
		$product->setName($Row[array_search ('Product name', $this->_attribute)]);
		$product->setDescription($Row[array_search ('description', $this->_attribute)]);
		$product->setAttributeSetId($this->getAttributeset($Row[array_search('attribute_set', $this->_attribute)])); // Attribute set id
		$product->setStatus(1); // Status on product enabled/ disabled 1/0
		$product->setWeight(0); // weight of product
		if($Row[array_search ('visibility', $this->_attribute)] == 'Not Visible Individually'){
			$visibility = 1;
		}
        else {
			$visibility = 4;
		}


		$product->setVisibility($visibility); // visibilty of product (catalog / search / catalog, search / Not visible individually)
		$product->setTaxClassId(0); // Tax class id
		$product->setTypeId('simple'); // type of product (simple/virtual/downloadable/configurable)
		$product->setPrice($Row[array_search('price', $this->_attribute)]); // price of product
		$product->setMetaTitle($Row[array_search('meta_title', $this->_attribute)]);
		$product->setMetaKeywords($Row[array_search('meta_keywords', $this->_attribute)]);
		$product->setMetaDescription($Row[array_search('meta_description', $this->_attribute)]);
        if($Row[array_search('additional_attributes', $this->_attribute)]){
			$additionalAttributes = explode(',',$Row[array_search('additional_attributes', $this->_attribute)]);
			if(count($additionalAttributes) > 0 ){
				foreach($additionalAttributes as $additionalAttribute) {
					$attribute = explode('=',$additionalAttribute);
					if(count($attribute) > 0 ){ 
						$entityType = 'catalog_product';
						//$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
						$attributedata = $this->_objectManager->get(\Magento\Eav\Model\Entity\Attribute::class)->loadByCode($entityType, $attribute[0]);
						if($attributedata->getFrontendInput()=='select'){
							$product->setData($attribute[0],$this->getDropdownattributevalue($product,$attribute[0],trim($attribute[1])));
						}
						else if($attributedata->getFrontendInput()=='multiselect'){ 
							$product->setData($attribute[0],$this->getMultiselectattributevalue($product,$attribute[0],trim($attribute[1])));
						}
						else{
							$product->setData($attribute[0],$attribute[1]);
						}
					}
				}
			}
		}
		
		$catIds ='';
		
		$_categoryFactory = $this->_objectManager->get('Magento\Catalog\Model\CategoryFactory');
		$cat = explode("|", $Row[array_search('categories', $this->_attribute)]);
		$parentId = '';
			foreach($cat as $title){
			$collection = $_categoryFactory->create()->getCollection();
			if($parentId){
				$collection->addFieldToFilter('parent_id', $parentId);
			}
			$collection->addFieldToFilter('name', ['in' => $title]);
			if ($collection->getSize()) {
					$categoryId = $collection->getFirstItem()->getId();
				
				if($catIds==''){
				$catIds = $categoryId;
				}else {
					$parentId = $categoryId;
					$catIds = $catIds.','.$categoryId;
				}
			}
			
		}
		if($catIds){
			$product->setCategoryIds(array($catIds));
		}
		
		$product->setStockData(
			array(
				'use_config_manage_stock' => 0,
				'manage_stock' => 1,
				'is_in_stock' => 1,
				'qty' => 100
			)
		);
		try
		{
		    $product->save();
		    $product->getId();
		}
		catch(Exception $e)
		{
		    
		}
		$this->imageUplaod($Row,$product);
	}


	public function imageUplaod($Row,$product){
		
		$dir = $this->_objectManager->get('\Magento\Framework\App\Filesystem\DirectoryList');
		$mediaImportFolder = $dir->getPath(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
		$product = $this->_objectManager->create('\Magento\Catalog\Model\Product')->load($product->getId());
		$productRepository = $this->_objectManager->create('\Magento\Catalog\Api\ProductRepositoryInterface');
		if($existingMediaGalleryEntries = $product->getMediaGalleryEntries()){
			foreach ($existingMediaGalleryEntries as $key => $entry) {
				unset($existingMediaGalleryEntries[$key]);
			}
			$product->setMediaGalleryEntries($existingMediaGalleryEntries);
			$productRepository->save($product); 
		}
		if($Row[array_search('image5', $this->_attribute)]){
			$imagenamewithpath = $mediaImportFolder.'/Images/'.$Row[array_search('image5', $this->_attribute)];  
			if(file_exists($imagenamewithpath)){
				$product->addImageToMediaGallery($imagenamewithpath, array('image', 'small_image', 'thumbnail'), false, false);
			}
		}
		if($Row[array_search('image4', $this->_attribute)]){
			$imagenamewithpath = $mediaImportFolder.'/Images/'.$Row[array_search('image4', $this->_attribute)];  
			if(file_exists($imagenamewithpath)){
				$product->addImageToMediaGallery($imagenamewithpath, array(), false, false);
			}
		}
		if($Row[array_search('image3', $this->_attribute)]){
			$imagenamewithpath = $mediaImportFolder.'/Images/'.$Row[array_search('image3', $this->_attribute)];  
			if(file_exists($imagenamewithpath)){
				$product->addImageToMediaGallery($imagenamewithpath, array(), false, false);
			}
		}
		if($Row[array_search('image2', $this->_attribute)]){
			$imagenamewithpath = $mediaImportFolder.'/Images/'.$Row[array_search('image2', $this->_attribute)];  
			if(file_exists($imagenamewithpath)){
				$product->addImageToMediaGallery($imagenamewithpath, array(), false, false);
			}
		}
		if($Row[array_search('image1', $this->_attribute)]){
			$imagenamewithpath = $mediaImportFolder.'/Images/'.$Row[array_search('image1', $this->_attribute)];  
			if(file_exists($imagenamewithpath)){
				$product->addImageToMediaGallery($imagenamewithpath, array(), false, false);
				
			}
		}
		$product->save();
		$objectManager4 = \Magento\Framework\App\ObjectManager::getInstance();
        $product = $objectManager4->create('Magento\Catalog\Model\Product')->load($product->getId());
        $productimages = $product->getMediaGalleryImages();
        $i = 0;
        
        foreach($productimages as $productimage)
        {
            if($i == count($productimages)-1){
              $imagePath = $productimage['file'];
              $objectManager4->get('Magento\Catalog\Model\Product\Action')->updateAttributes([0 => $product->getId()], array("thumbnail" => $imagePath,"small_image" => $imagePath), 0);
              $product->setSmallImage($imagePath);
              $product->setImage($imagePath);
              $product->setThumbnail($imagePath);
              $product->save();
              break;
            }
            $i++;
        }
	}

	public function getMultiselectattributevalue($_product,$attributeCode, $optionLabel){
		if($optionLabel){
			$optionIds = array();
			$label = explode('|',$optionLabel);
			foreach($label as $value){
				$optionIds[] = $this->getDropdownattributevalue($_product,$attributeCode, $value);	
			}
			return $optionIds;
		}
	}
	public function getDropdownattributevalue($_product,$attributeCode, $optionLabel){
		
		if($optionLabel){
			if($id = $this->getOptionIdByLabel($_product,$attributeCode,$optionLabel)){
				return $id;
			}
			else {
				$this->addoption($attributeCode,$optionLabel);
				return $this->getOptionIdByLabel($_product,$attributeCode,$optionLabel);
			}
		
		}
	}
	public function getOptionIdByLabel($_product,$attributeCode,$optionLabel)
    {
		
		$isAttributeExist = $_product->getResource()->getAttribute($attributeCode);
        $optionId = '';
        if ($isAttributeExist && $isAttributeExist->usesSource()) {
            $optionId = $isAttributeExist->getSource()->getOptionId($optionLabel);
        }
        return $optionId;
	}
	
	public function addoption($attributeCode,$label){
		$attr = $this->_objectManager->create('\Magento\Eav\Model\Entity\Attribute'); 
		$attr->loadByCode('catalog_product',$attributeCode); 
		$option = []; 
		$option['value']['any_option_name'][]= $label; 
		$attr->addData(array('option' => $option));
		try
		{
		   $attr->save();
		}
		catch(Exception $e)
		{
		   echo $e; 
		}
	}
}

