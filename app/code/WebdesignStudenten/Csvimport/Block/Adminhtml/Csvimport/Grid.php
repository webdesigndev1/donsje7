<?php
namespace WebdesignStudenten\Csvimport\Block\Adminhtml\Csvimport;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var \WebdesignStudenten\Csvimport\Model\csvimportFactory
     */
    protected $_csvimportFactory;

    /**
     * @var \WebdesignStudenten\Csvimport\Model\Status
     */
    protected $_status;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \WebdesignStudenten\Csvimport\Model\csvimportFactory $csvimportFactory
     * @param \WebdesignStudenten\Csvimport\Model\Status $status
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \WebdesignStudenten\Csvimport\Model\CsvimportFactory $csvimportFactory,
        \WebdesignStudenten\Csvimport\Model\Status $status,
        \Magento\Framework\Module\Manager $moduleManager,
        array $data = []
    ) {
        $this->_csvimportFactory = $csvimportFactory;
        $this->_status = $status;
        $this->moduleManager = $moduleManager;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('csvimportGrid');
        $this->setDefaultSort('csvimport_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->_csvimportFactory->create()->getCollection();
        $this->setCollection($collection);

        parent::_prepareCollection();
        return $this;
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'csvimport_id',
            [
                'header' => __('ID'),
                'type' => 'number',
                'index' => 'csvimport_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id',
            ]
        );
                
        $this->addColumn(
            'file',
            [
                'header' => __('File Name'),
                'index' => 'file'
            ]
        );
        
        $this->addColumn(
            'title',
            [
                'header' => __('Title'),
                'index' => 'title',
                'class' => 'xxx'
            ]
        );

        $this->addColumn(
            'is_active',
            [
                'header' => __('Import'),
                'index' => 'is_active',
                'type' => 'options',
                'options' => $this->_status->getOptionArray()
            ]
        );

        $this->addColumn(
            'edit',
            [
                'header' => __('Import'),
                'type' => 'action',
                'getter' => 'getId',
                'actions' => [
                    [
                        'caption' => __('Import'),
                        'url' => [
                            'base' => '*/*/import'
                        ],
                        'field' => 'csvimport_id'
                    ]
                ],
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'header_css_class' => 'col-action',
                'column_css_class' => 'col-action'
            ]
        );

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

    

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('csvimport/*/grid', ['_current' => true]);
    }

    /**
     * @param \WebdesignStudenten\Csvimport\Model\csvimport|\Magento\Framework\Object $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl(
            'csvimport/*/edit',
            ['csvimport_id' => $row->getId()]
        );
    }
}
