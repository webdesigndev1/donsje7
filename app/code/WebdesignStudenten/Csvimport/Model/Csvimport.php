<?php
namespace WebdesignStudenten\Csvimport\Model;

class Csvimport extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('WebdesignStudenten\Csvimport\Model\ResourceModel\Csvimport');
    }
}
