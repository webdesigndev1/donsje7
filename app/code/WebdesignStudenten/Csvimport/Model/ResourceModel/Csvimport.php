<?php
namespace WebdesignStudenten\Csvimport\Model\ResourceModel;

class Csvimport extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('csvimport', 'csvimport_id');
    }
}
