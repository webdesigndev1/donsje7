<?php
/**
 * Admin can sync customer, products, sales, cart, newsletter subscribers, wishlist etc.
 * Copyright (C) 2019  
 * 
 * This file is part of WebdesignStudenten/SkiefRestAPI.
 * 
 * WebdesignStudenten/SkiefRestAPI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace WebdesignStudenten\SkiefRestAPI\Model;

use WebdesignStudenten\SkiefRestAPI\Api\Data\DataScopeInterface;
use WebdesignStudenten\SkiefRestAPI\Api\Data\DataScopeInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class DataScope extends \Magento\Framework\Model\AbstractModel
{

    protected $datascopeDataFactory;

    protected $dataObjectHelper;

    protected $_eventPrefix = 'webdesignstudenten_skiefrestapi_datascope';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param DataScopeInterfaceFactory $datascopeDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \WebdesignStudenten\SkiefRestAPI\Model\ResourceModel\DataScope $resource
     * @param \WebdesignStudenten\SkiefRestAPI\Model\ResourceModel\DataScope\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        DataScopeInterfaceFactory $datascopeDataFactory,
        DataObjectHelper $dataObjectHelper,
        \WebdesignStudenten\SkiefRestAPI\Model\ResourceModel\DataScope $resource,
        \WebdesignStudenten\SkiefRestAPI\Model\ResourceModel\DataScope\Collection $resourceCollection,
        array $data = []
    ) {
        $this->datascopeDataFactory = $datascopeDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve datascope model with datascope data
     * @return DataScopeInterface
     */
    public function getDataModel()
    {
        $datascopeData = $this->getData();
        
        $datascopeDataObject = $this->datascopeDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $datascopeDataObject,
            $datascopeData,
            DataScopeInterface::class
        );
        
        return $datascopeDataObject;
    }
}
