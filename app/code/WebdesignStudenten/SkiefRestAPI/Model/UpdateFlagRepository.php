<?php
/**
 * Admin can sync customer, products, sales, cart, newsletter subscribers, wishlist etc.
 * Copyright (C) 2019  
 * 
 * This file is part of WebdesignStudenten/SkiefRestAPI.
 * 
 * WebdesignStudenten/SkiefRestAPI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace WebdesignStudenten\SkiefRestAPI\Model;

use WebdesignStudenten\SkiefRestAPI\Api\UpdateFlagRepositoryInterface;
use WebdesignStudenten\SkiefRestAPI\Api\Data\UpdateFlagSearchResultsInterfaceFactory;
use WebdesignStudenten\SkiefRestAPI\Api\Data\UpdateFlagInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use WebdesignStudenten\SkiefRestAPI\Model\ResourceModel\UpdateFlag as ResourceUpdateFlag;
use WebdesignStudenten\SkiefRestAPI\Model\ResourceModel\UpdateFlag\CollectionFactory as UpdateFlagCollectionFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\ExtensibleDataObjectConverter;

class UpdateFlagRepository implements UpdateFlagRepositoryInterface
{

    protected $resource;

    protected $updateFlagFactory;

    protected $updateFlagCollectionFactory;

    protected $searchResultsFactory;

    protected $dataObjectHelper;

    protected $dataObjectProcessor;

    protected $dataUpdateFlagFactory;

    protected $extensionAttributesJoinProcessor;

    private $storeManager;

    private $collectionProcessor;

    protected $extensibleDataObjectConverter;

    /**
     * @param ResourceUpdateFlag $resource
     * @param UpdateFlagFactory $updateFlagFactory
     * @param UpdateFlagInterfaceFactory $dataUpdateFlagFactory
     * @param UpdateFlagCollectionFactory $updateFlagCollectionFactory
     * @param UpdateFlagSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceUpdateFlag $resource,
        UpdateFlagFactory $updateFlagFactory,
        UpdateFlagInterfaceFactory $dataUpdateFlagFactory,
        UpdateFlagCollectionFactory $updateFlagCollectionFactory,
        UpdateFlagSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->updateFlagFactory = $updateFlagFactory;
        $this->updateFlagCollectionFactory = $updateFlagCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataUpdateFlagFactory = $dataUpdateFlagFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \WebdesignStudenten\SkiefRestAPI\Api\Data\UpdateFlagInterface $updateFlag
    ) {
        /* if (empty($updateFlag->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $updateFlag->setStoreId($storeId);
        } */
        
        $updateFlagData = $this->extensibleDataObjectConverter->toNestedArray(
            $updateFlag,
            [],
            \WebdesignStudenten\SkiefRestAPI\Api\Data\UpdateFlagInterface::class
        );
        
        $updateFlagModel = $this->updateFlagFactory->create()->setData($updateFlagData);
        
        try {
            $this->resource->save($updateFlagModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the updateFlag: %1',
                $exception->getMessage()
            ));
        }
        return $updateFlagModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getById($updateFlagId)
    {
        $updateFlag = $this->updateFlagFactory->create();
        $this->resource->load($updateFlag, $updateFlagId);
        if (!$updateFlag->getId()) {
            throw new NoSuchEntityException(__('UpdateFlag with id "%1" does not exist.', $updateFlagId));
        }
        return $updateFlag->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->updateFlagCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \WebdesignStudenten\SkiefRestAPI\Api\Data\UpdateFlagInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \WebdesignStudenten\SkiefRestAPI\Api\Data\UpdateFlagInterface $updateFlag
    ) {
        try {
            $updateFlagModel = $this->updateFlagFactory->create();
            $this->resource->load($updateFlagModel, $updateFlag->getUpdateflagId());
            $this->resource->delete($updateFlagModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the UpdateFlag: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($updateFlagId)
    {
        return $this->delete($this->getById($updateFlagId));
    }
}
