<?php
/**
 * Admin can sync customer, products, sales, cart, newsletter subscribers, wishlist etc.
 * Copyright (C) 2019  
 * 
 * This file is part of WebdesignStudenten/SkiefRestAPI.
 * 
 * WebdesignStudenten/SkiefRestAPI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace WebdesignStudenten\SkiefRestAPI\Model;

use WebdesignStudenten\SkiefRestAPI\Api\Data\UpdateDateInterface;
use WebdesignStudenten\SkiefRestAPI\Api\Data\UpdateDateInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class UpdateDate extends \Magento\Framework\Model\AbstractModel
{

    protected $updatedateDataFactory;

    protected $dataObjectHelper;

    protected $_eventPrefix = 'webdesignstudenten_skiefrestapi_updatedate';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param UpdateDateInterfaceFactory $updatedateDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \WebdesignStudenten\SkiefRestAPI\Model\ResourceModel\UpdateDate $resource
     * @param \WebdesignStudenten\SkiefRestAPI\Model\ResourceModel\UpdateDate\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        UpdateDateInterfaceFactory $updatedateDataFactory,
        DataObjectHelper $dataObjectHelper,
        \WebdesignStudenten\SkiefRestAPI\Model\ResourceModel\UpdateDate $resource,
        \WebdesignStudenten\SkiefRestAPI\Model\ResourceModel\UpdateDate\Collection $resourceCollection,
        array $data = []
    ) {
        $this->updatedateDataFactory = $updatedateDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve updatedate model with updatedate data
     * @return UpdateDateInterface
     */
    public function getDataModel()
    {
        $updatedateData = $this->getData();
        
        $updatedateDataObject = $this->updatedateDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $updatedateDataObject,
            $updatedateData,
            UpdateDateInterface::class
        );
        
        return $updatedateDataObject;
    }
}
